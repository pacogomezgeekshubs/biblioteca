class AuthorsController < ApplicationController
    def newAuthor
        @author=Author.new
        @author.nombre=params[:name]
        @author.save
        redirect_to authors_path
    end
    def index
        @authors=Author.all
    end
    def show
        @author=Author.find(params[:id])
    end
end
