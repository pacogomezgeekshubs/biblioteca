class BooksController < ApplicationController
    def newBook
        @book=Book.new
        @book.titulo=params[:title]
        @book.author_id=params[:idAuthor]
        @book.save
        redirect_to books_path
    end
    def index
        @books=Book.all
    end
    def new
        @book=Book.new
        @book.author_id=1
    end
    def show
        @book=Book.find(params[:id])
    end
    def create
        book = Book.create!(book_params)
        redirect_to books_path
    end
    private
    def book_params
      params.require(:book).permit(:titulo, :author_id, :photo)
    end
        
end
