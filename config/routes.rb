Rails.application.routes.draw do
  ##### RESOURCES
  resources :books
  resources :authors do
    resources :books
  end
  #####
  get "/books/newBook/:title/:idAuthor", to: "books#newBook", as: "newBook"
  get "/authors/newAuthor/:name", to: "authors#newAuthor", as:"newAuthor"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
